$(function(){
  /**
   * 公共
   */
  var user_id = getCookie('userId')
  console.log(user_id)
  /**
   * all check
   */
  $('.all-check').on('click',function(){
    var checked = $(this).prop('checked')
    $('.all-check').prop('checked', checked)
    $('.single-check').prop('checked', checked)
  })
  $('.single-check').on('click', function(){

  })
  /**
   * 数量改变
   */
  $('.cart-minus').on('click', function(){
    var product = $(this).parent().find('.cart-pro_num');
    if ($(this).parent().hasClass('cart-disabled')) {
      return
    }
    var val = product.text();
    if(val > 1) {
      product.html(parseInt(val) - 1);
    }
  })
  $('.cart-plus').on('click', function(){
    var product = $(this).parent().find('.cart-pro_num');
    if ($(this).parent().hasClass('cart-disabled')) {
      return
    }
    var val = product.text();
    if(val < 999) {
      product.html(parseInt(val) + 1)
    }    
  })
  /**
   * 更新商品数量
   */
  var updateProNum = function(id, num){
    $.ajax({
      url: '/BeMoralOfficial/cart/updateCartNum.do',
      type: 'POST',
      data: {
        userId: user_id || "",
        cId: id,
        cNum: num
      },
      dataType: 'JSON',
      success: function(data) {
        if(data.status == 0) {

        } else {
          
        }
      },
      error: function() {

      }
    })
  }
  /**
   * 购物车商品列表
   */
  var getCartList = function() {
    $.ajax({
      url: '/BeMoralOfficial/cart/getCartAll.do',
      type: 'POST',
      data: {
        userId: user_id || ""
      },
      dataType: 'JSON',
      success: function(data) {
        if(data.status == 0) {

        } else {

        }
      },
      error: function() {

      }
    })
  }
  getCartList()
  /**
   * 删除购物车商品 全部
   */
  var removeAll = function() {
    $.ajax({
      url: '/BeMoralOfficial/cart/delCartAll.do',
      type: 'POST',
      data: {
        userId: user_id || ""
      },
      dataType: 'JSON',
      success: function(data) {
        if(data.status == 0) {

        } else {

        }
      },
      error: function(){

      }
    })
  }
  /**
   * 删除单个商品
   */
  $('.cart-remove').on('click', function(){
    var pro_id = $(this).data('id') || 1;
    var that = $(this).parents('tr')
    that.remove()
    $.ajax({
      url: '/BeMoralOfficial/cart/delCart.do',
      type: 'POST',
      data: {
        userId: user_id || "",
        cId: cid
      },
      dataType: 'JSON',
      success: function(data) {
        if(data.status == 0) {
          that.remove()
        } else {

        }
      },
      error: function(){
        
      }
    })
  })
})