var browser={  
  versions:function(){   
   var u = navigator.userAgent, app = navigator.appVersion;   
   return {
      trident: u.indexOf('Trident') > -1, //IE内核  
      presto: u.indexOf('Presto') > -1, //opera内核  
      webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核  
      gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核  
      mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端  
      ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端  
      android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器  
      iPhone: u.indexOf('iPhone') > -1 , //是否为iPhone或者QQHD浏览器  
      iPad: u.indexOf('iPad') > -1, //是否iPad    
      webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部  
      weixin: u.indexOf('MicroMessenger') > -1, //是否微信   
      qq: u.match(/\sQQ/i) == " qq" //是否QQ  
    };  
 }(),  
 language:(navigator.browserLanguage || navigator.language).toLowerCase()  
}

$(function(){
	if(browser.versions.mobile || browser.versions.android || browser.versions.iPhone || browser.versions.iPad){ 
		//导航栏
		  var  nav_lock = false;
		  $(".nav_button").on("touchend",function(){
		    if (nav_num==0&&nav_lock==false) {
		      nav_num=1;
		      nav_lock=true;
		  /*    $('.nav_button').css("position","fixed");*/
		      $(".logo_m1,.cart_box1").hide();
		      $(".nav_list").slideDown(200);
		      $("#first_menu_m").removeClass("fadeOut");
		      $(".nav_button").eq(0).stop().transition({ rotate:90,translate:[0,2] }, 300, 'linear', function(){
		        $("#first_menu_m").addClass("fadeInDown");
		        $(this).find("span").stop().transition({ rotate:45 }, 100, 'linear', function(){
		          nav_lock=false;
		          unScroll();
		          
		        });
		      });
		      $(".nav_button").eq(1).stop().transition({ rotate:-90,translate:[0,2] }, 300, 'linear', function(){
		        $(this).find("span").stop().transition({ rotate:-45 }, 100, 'linear', function(){});
		      });
		    }else if (nav_num==1&&nav_lock==false) {
		      nav_num=0;
		      nav_lock=true;
		    /*  $('.nav_button').css("position","absolute");*/
		      $(".nav_list").slideUp();
		      $(".logo_m1,.cart_box1").show();
		      $("#first_menu_m").removeClass("fadeInDown").addClass("fadeOut").css("opacity","0");
		      $("#first_menu_m>li>ul").slideUp();
		      $(".nav_button").eq(0).find("span").stop().transition({ rotate:0}, 100, 'linear', function(){
		        $(".nav_button").eq(0).stop().transition({ rotate:0,translate:[0,0] }, 300, 'linear', function(){
		          nav_lock=false;
		          reScroll();
		        });
		      });

		      $(".nav_button").eq(1).find("span").stop().transition({ rotate:0}, 100, 'linear', function(){
		        $(".nav_button").eq(1).stop().transition({ rotate:0,translate:[0,0] }, 300, 'linear', function(){});
		      });
		    }
		  })

		  $("#first_menu_m>li>a").on("touchend",function(){
		    $(this).siblings("ul").slideToggle();
		  })


		//底部导航栏
		  $(".nav_button1").on("touchend",function(){
		    if (nav_num1==0&&nav_lock1==false) {
		      nav_num1=1;
		      nav_lock1=true;
		      $(".nav_list1").slideDown(200);
		      $("#bottom_menu_m").removeClass("fadeOut");
		      $(".nav_button1").eq(0).stop().transition({ rotate:90,translate:[0,2] }, 300, 'linear', function(){
		        //$("#bottom_menu_m").slideDown(200);
		        $("#bottom_menu_m").addClass("fadeInUp");
		        $(this).find("span").stop().transition({ rotate:45 }, 100, 'linear', function(){
		          nav_lock1=false;
		          
		        });
		      });
		      $(".nav_button1").eq(1).stop().transition({ rotate:-90,translate:[0,2] }, 300, 'linear', function(){
		        $(this).find("span").stop().transition({ rotate:-45 }, 100, 'linear', function(){});
		      });
		    }else if (nav_num1==1&&nav_lock1==false) {
		      nav_num1=0;
		      nav_lock1=true;
		      $(".nav_list1").slideUp();
		      //$("#bottom_menu_m").hide();
		      $("#bottom_menu_m").removeClass("fadeInUp").addClass("fadeOut").css("opacity","0");
		      $("#bottom_menu_m>li>ul").slideUp();
		      $(".nav_button1").eq(0).find("span").stop().transition({ rotate:0}, 100, 'linear', function(){
		        $(".nav_button1").eq(0).stop().transition({ rotate:0,translate:[0,0] }, 300, 'linear', function(){
		          nav_lock1=false;
		          
		        });
		      });

		      $(".nav_button1").eq(1).find("span").stop().transition({ rotate:0}, 100, 'linear', function(){
		        $(".nav_button1").eq(1).stop().transition({ rotate:0,translate:[0,0] }, 300, 'linear', function(){});
		      });
		    }
		  })

		  $("#bottom_menu_m>li>a").on("touchend",function(){
		    $(this).siblings("ul").slideToggle();
		  })
	}
})