$(function(){

	//商品系列页
    var trade_kind_introduce = function(){
        var get_language = getQueryString("language");
        var get_preview = getQueryString("preview");
        var get_trade_kind = decodeURI(getQueryString("trade_kind"));
        var get_trade_series = decodeURI(getQueryString("trade_series"));
        console.log(get_language+" "+get_preview+" "+get_trade_kind+" "+get_trade_series);
        $.ajax({
            url: "/BeMoralOfficial/official/trade_kind_introduce.do",
            type: "POST",
            data:{language:get_language,preview:get_preview,trade_kind:get_trade_kind,trade_series:get_trade_series},
            cache: false,
            dataType:"JSON",
            success: function (data) {
                if(data.status==0){
                    console.log("商品系列页",data.data);
                    //$(".succession_box").html(data.data.conten);
                    $("#succession .tuijian_button").html(get_trade_series);
                    $(".succession_box .left_tu").attr("src",data.data.trade_photo4);
                    $(".succession_box .details_unit").eq(0).find(".details_unit_words").html('<p>'+data.data.desc1+'</p>');
                    $(".succession_box .right_tu").attr("src",data.data.trade_photo4);
                    $(".succession_box .details_unit").eq(1).find(".details_unit_words").html('<p>'+data.data.desc2+'</p>');
                    $(".succession_box .content_left").html(data.data.story_desc1);
                    $(".succession_box .content_right").html(data.data.story_desc2);
                    $(".succession_bg img").attr("src", data.data.trade_photo1);
                    if (data.data.trade_url_link !=null) {
/*                      $("#product_video #example_video_1 source").attr("src",data.data.trade_url_link);
                      $("#product_video #example_video_1").attr("src",data.data.trade_url_link);*/
                      /*$("#example_video_1 source").attr("src",data.data.trade_url_link);*/
                      /*$("#example_video_1").attr("src",data.data.trade_url_link);*/
                      $(".video_box").append(data.data.trade_url_link);
                      $(".video_box embed").attr("id","example_video_1");
                      $(".video_box iframe").attr("id","example_video_1");
                    }else{
                      $(".video_box").hide();
                    }
                    
                    $(".succession_bg>img").addClass("fadeInDown");
                    setTimeout(function(){
                      $(".big_title").addClass("fadeInDown");
                      $(".sma_title").addClass("fadeInDown");
                    },500)
                
/*                var myPlayer = videojs('example_video_1');
                videojs("example_video_1", {}, function(){
                  // Player (this) is initialized and ready.
                });

                myPlayer.on("pause", function(){
                  // $(".vjs-big-button").hide();
                });


                myPlayer.on("play", function(){
                  $(".vjs-big-button").hide();
                });*/
                }else{
                    console.log("服务器繁忙")
                }
                
               
            },
            error: function () {
                //alert(111)
            }
        });
      }
      

	
	



    trade_kind_introduce();


/*$(window).scroll(function(){
  // console.log(document.body.scrollTop || document.documentElement.scrollTop);
  Global.html_scroll();
})

$(".fix_none").addClass('animated');
var nav_lock = false;
Global.html_scroll=function(){
  if (document.body.scrollTop || document.documentElement.scrollTop>=$(".footer").offset().top-$(".footer").height()-300) {
    $(".copy_right").addClass("fadeInUp");
    $(".footer_list>li>span").addClass("fadeInUp");
    $(".footer_list>li>ul>li>a").addClass("fadeInUp");
    setTimeout(function(){
      $(".search_box").addClass("fadeIn");
    },300)
  }



  if (document.body.scrollTop || document.documentElement.scrollTop>=20) {
    nav_lock = false;
    setTimeout(function(){
      $(".second_menu").hide();
      $("#first_menu a").removeClass("active");
      $(".bottom_line,.bottom_line1").width(0);
      $(".second_menu_ul li a").removeClass("active");
      $(".fix_none").addClass('fadeOut');
      $(".nav_box").css("position","fixed");
      $(".nav_box").css("top","0px");
      $(".nav_box").stop().animate({height:"80px"},50);
      $(".nav_box").on('mouseenter',function(){
        if (nav_lock==true) {
          return false;
        }
        $(".fix_none").removeClass('fadeOut').addClass('fadeIn');
        $(".nav_box").css("top","0px");
        $(".nav_box").css("position","fixed");
        $(".nav_box").stop().animate({height:"140"},50);
        $(".second_menu").css("position","fixed");
      })
    },1)
  }else if(document.body.scrollTop || document.documentElement.scrollTop<20){
    nav_lock = true;
    setTimeout(function(){
      $(".second_menu").css("position","absolute");
      $(".fix_none").removeClass('fadeOut').addClass('fadeIn');
      $(".nav_box").css("top","40px");
      $(".nav_box").css("position","absolute");
      $(".nav_box").stop().animate({height:"140"},50);
    },1)
    
  }
}

Global.html_scroll();*/

Global.jiantou(".left_jt_succession");





Index_product_details();
})


var Index_product_details = function(swiper_el){
	var get_language = getQueryString("language");
	var get_preview = getQueryString("preview");
	var get_trade_kind = decodeURI(getQueryString("trade_kind"));
	var get_trade_series = decodeURI(getQueryString("trade_series"));
	console.log(get_language+" "+get_preview+" "+get_trade_kind);
	$.ajax({
		url: "/BeMoralOfficial/official/gettrade_details.do",
		type: "POST",
		data:{language:"tc",preview:2,trade_kind:get_trade_kind},
		cache: false,
		dataType:"JSON",
		success: function (data) {
			if(data.status==0){
				console.log("主要商品详细",data.data);
				
				data.data.forEach(function(element){
					console.log(element.trade_series+" "+get_trade_series);
					if (element.trade_series==get_trade_series) {
						$("#succession #swiper3 .swiper-wrapper").append(
								'<div class="swiper-slide" data-id='+element.id+' trade_kind='+element.trade_kind+'>'
								+'<img class="center-block hidden-xs hidden-sm" src='+element.trade_photo1+' />'
								+'<img class="center-block hidden-md hidden-lg" src='+element.trade_photo1+' />'
								+'<div class="des_box">'
								+'<div class="product_name">'+element.title+'</div>'
								+'<div class="product_weight">'+element.standard+'</div>'
								+'<div class="product_stars"><img src="images/star.png" /><img src="images/star.png" /><img src="images/star.png" /><img src="images/star.png" /><img src="images/half_star.png" /><span>4.5/5</span></div>'
								+'<div class="product_price">NT$'+element.sell_price+'</div>'
								+'</div>'
								+'</div>')
					}
				})
				// swiper_el.update();
				var  mySwiper3 = new Swiper('#swiper3', {
					slidesPerView: 3,//一行显示X个
					slidesPerColumnFill : 'row',
					slidesPerColumn: Math.ceil(data.data.length/3),//显示X行
					centeredSlides : false,
					allowTouchMove: false,
					breakpoints: { 
						// 1600: {
						//   slidesPerView: 3,
						//   loopedSlides: 3,
						// }
					},
					on:{
						init:function(){
							// Index_product_details(this);
							$("#succession #swiper3").on("click",".swiper-slide",function(){
								var this_id = $(this).attr("data-id");
								var this_kind =  $(this).attr("trade_kind");
								window.location.href=encodeURI(encodeURI('product_details.html?id='+this_id+'&preview=2&language=tc'+'&trade_kind='+this_kind));
							})
							
							
							$(".product_banner .swiper-pagination-bullets").css("marginLeft",(-$(".product_banner .swiper-pagination-bullets").width()/2)+"px");
							$(".product_swiper_box").css("marginTop",-$(".product_swiper_box").height()/2);
						},
						resize: function(){
							this.updateSize();
							if ($(window).width()<=1600) {
								$(".show_box_title").css("line-height","100%");
							}else{
								$(".show_box_title").css("line-height","150%");
							}
							
							if ($(window).width()<=1700) {
								$("#swiper2 .swiper-slide.swiper-slide-prev .tab_unit").addClass("w_1700");
								$("#swiper2 .swiper-slide.swiper-slide-next .tab_unit").addClass("w_1700");
								$(".tab_unit").addClass("w_1700");
								/*               $("#swiper2 .swiper-slide .tab_unit").addClass("w_1700");*/
							}else{
								$("#swiper2 .swiper-slide.swiper-slide-prev .tab_unit").removeClass("w_1700");
								$("#swiper2 .swiper-slide.swiper-slide-next .tab_unit").removeClass("w_1700");
								$(".tab_unit").removeClass("w_1700");
							}
							
						},
						slidePrevTransitionStart:function(){
							
						},
						slideNextTransitionStart:function(){
							
						},
					},
				})
				
				
				
			}else{
				console.log("服务器繁忙")
			}
			
			
		},
		error: function () {
			//alert(111)
		}
	});
}
